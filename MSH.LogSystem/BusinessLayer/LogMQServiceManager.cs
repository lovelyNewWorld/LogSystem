﻿using BusinessLayer.Interface;
using Common;
using Configuration;
using DTO;
using ElasticSearchAccess;
using Rabbitmq.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class LogMQServiceManager : ILogMQServiceManager
    {
        private static int QueueCount = 500;
        private InfoLogAccess _InfoLogAccess = new InfoLogAccess();
        private ErrorLogAccess _ErrorLogAccess = new ErrorLogAccess();
        private WarnLogAccess _WarnLogAccess = new WarnLogAccess();
        private DebugLogAccess _DebugLogAccess = new DebugLogAccess();
        private BlockingCollection<QueueData> LogRequests = new BlockingCollection<QueueData>(QueueCount);
        private List<QueueData> QueueDatas = new List<QueueData>();
        public event Action<LogRequest> MessageReceivedEvent;

        public string AppId { get; set; }

        public LogMQServiceManager()
        {
            RabbitMqMessageManage.MessageReceivedEvent = MessageConsumer_MessageReceivedEvent;
            //Task.Factory.StartNew(() =>
            //{
            //    List<QueueData> QueueDatas = new List<QueueData>();
            //    foreach (var item in LogRequests.GetConsumingEnumerable())
            //    {
            //        QueueDatas.Add(item);
            //        if ((QueueDatas.Count == QueueCount || LogRequests.Count == 0) &&
            //        QueueDatas.Count > 0)
            //        {
            //            //保存
            //            SaveLog(QueueDatas);
            //            QueueDatas.Clear();
            //        }
            //    }
            //});
        }

        #region 向队列插入日志

        public void SendErrorLog(LogRequest request)
        {
            request.Send(LogLevel.Error.ToString());
        }

        public void SendInfoLog(LogRequest request)
        {
            request.Send(LogLevel.Info.ToString());
        }

        public void SendWarnLog(LogRequest request)
        {
            request.Send(LogLevel.Warn.ToString());
        }

        public void SendDebugLog(LogRequest request)
        {
            request.Send(LogLevel.Debug.ToString());
        }

        #endregion

        #region 从队列中接收消息

        public LogRequest GetLog(LogLevel level)
        {
            //var queueName = level.ToString();
            //var obj = RabbitMqMessageManage.Get<LogRequest>(queueName);
            //return obj;
            return null;
        }

        public void StartGetMsg(LogLevel level)
        {
            var queueName = level.ToString();
            RabbitMqMessageManage.StartGet<LogRequest>(queueName);
        }

        /// <summary>
        /// 处理发送来的日志
        /// </summary>
        /// <param name="obj"></param>
        private void MessageConsumer_MessageReceivedEvent(object obj, string queueName, ulong deliveryTag)
        {
            var log = obj as LogRequest;
            if (log == null)
            {
                //让消息重新回到队列
                RabbitMqMessageManage.SendReceivedResult(queueName, deliveryTag, false);
                Logger.Error($"队列消息反序列化失败:{obj.ToJson()}");
                return;
            }
            var res = SaveLog(queueName, log);
            if (res)
                RabbitMqMessageManage.SendReceivedResult(queueName, deliveryTag, true);
            else
                RabbitMqMessageManage.SendReceivedResult(queueName, deliveryTag, false);
        }

        /// <summary>
        /// 批量保存日志
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="logRequest"></param>
        /// <returns></returns>
        private bool SaveLog(List<QueueData> QueueDatas)
        {
            try
            {
                var infoRequests = QueueDatas.Where(a => a.QueueName == LogLevel.Info.ToString()).Select(a => a.LogRequest).ToList();
                var errorRequests = QueueDatas.Where(a => a.QueueName == LogLevel.Error.ToString()).Select(a => a.LogRequest).ToList();
                var warnRequests = QueueDatas.Where(a => a.QueueName == LogLevel.Warn.ToString()).Select(a => a.LogRequest).ToList();
                var debugRequests = QueueDatas.Where(a => a.QueueName == LogLevel.Debug.ToString()).Select(a => a.LogRequest).ToList();
                if (infoRequests != null && infoRequests.Count > 0)
                    _InfoLogAccess.AddLog(infoRequests);
                if (errorRequests != null && errorRequests.Count > 0)
                    _ErrorLogAccess.AddLog(errorRequests);
                if (warnRequests != null && warnRequests.Count > 0)
                    _WarnLogAccess.AddLog(warnRequests);
                if (debugRequests != null && debugRequests.Count > 0)
                    _DebugLogAccess.AddLog(debugRequests);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error($"保存日志发生错误:{ex}  日志内容:{QueueDatas.ToJson()}");
                return false;
            }
        }

        /// <summary>
        /// 返回值表示是否需要标记已处理
        /// </summary>
        private bool SaveLog(string queueName, LogRequest logRequest)
        {
            try
            {
                if (queueName.Equals(LogLevel.Info.ToString()))
                {
                    _InfoLogAccess.AddLog(logRequest);
                    return true;
                }
                if (queueName.Equals(LogLevel.Error.ToString()))
                {
                    _ErrorLogAccess.AddLog(logRequest);
                    return true;
                }
                if (queueName.Equals(LogLevel.Warn.ToString()))
                {
                    _WarnLogAccess.AddLog(logRequest);
                    return true;
                }
                if (queueName.Equals(LogLevel.Debug.ToString()))
                {
                    _DebugLogAccess.AddLog(logRequest);
                    return true;
                }
                return false;
            }
            catch (AppIdInvalidException ex)
            {
                Logger.Error($"{nameof(logRequest.AppId)}:{logRequest.AppId} 保存日志发生错误:{ex}  日志内容:{logRequest.ToJson()}");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error($"{nameof(logRequest.AppId)}:{logRequest.AppId} 保存日志发生错误:{ex}  日志内容:{logRequest.ToJson()}");
                return false;
            }
        }

        #endregion
    }
}
