﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TcpServiceInfo
    {
        /// <summary>
        /// 允许最大连接数
        /// </summary>
        public long MaxConnectionNumber { get; set; }

        /// <summary>
        /// 是否正在运行
        /// </summary>
        public bool IsRunning { get; set; }

        /// <summary>
        /// 开启时间
        /// </summary>
        public DateTime StartedTime { get; set; }

        /// <summary>
        /// 当前连接数
        /// </summary>
        public long TotalConnections { get; set; }

        /// <summary>
        /// 接收请求速度
        /// </summary>
        public string RequestHandlingSpeed { get; set; }

        /// <summary>
        /// 总共接收到的请求数
        /// </summary>
        public string TotalHandledRequests { get; set; }

        /// <summary>
        /// 可用队列数
        /// </summary>
        public long AvialableSendingQueueItems { get; set; }

        /// <summary>
        /// 队列总数
        /// </summary>
        public string TotalSendingQueueItems { get; set; }

        /// <summary>
        /// 信息收集的时间
        /// </summary>
        public DateTime CreatTime { get; set; }

        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName { get; set; }
    }
}
