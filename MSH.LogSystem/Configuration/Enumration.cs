﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    /// <summary>
    /// 日志级别
    /// </summary>
    [Flags]
    public enum LogLevel : int
    {
        [Note("信息")]
        Info = 1,
        [Note("调试")]
        Debug = Info * 2,
        [Note("警告")]
        Warn = Debug * 2,
        [Note("错误")]
        Error = Warn * 2,
    }

    /// <summary>
    /// 匹配规则
    /// </summary>
    [Flags]
    public enum MatchingRule : int
    {
        [Note("包含")]
        Contain = 1,
        [Note("等于")]
        Equal = Contain * 2,
        [Note("正则")]
        Regular = Equal * 2
    }
}
